Photo Grabber 
====
A simple Rust app to pull some training data for an idea. Currently just trying
out Unsplash api

Setup
===
* Setup your app on Unsplash
* Make a `config.json` file in the root of this project with the following format
```json

{
  "access": "<your access token>"
}

```
* Currently only pulls from editorial feed. 

Useful links
===
* https://docs.rs/reqwest/0.11.6/reqwest/
