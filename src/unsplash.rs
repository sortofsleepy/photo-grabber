use image::GenericImageView;
use std::path::Path;

/// Basic wrapper into the Unsplash api - focused on making requests
/// that don't require user auth.
pub struct Unsplash {
    config:serde_json::Value,
    base_url:String,
    client:reqwest::Client,
    image_urls:Vec<String>
}

impl Unsplash{
    pub fn new(config:serde_json::Value)->Self {
        Unsplash{
            config,
            base_url:String::from("https://api.unsplash.com"),
            client:reqwest::Client::new(),
            image_urls:vec![]
        }
    }

    /// returns client id parameter
    fn get_client_id(&self)->String {

        let key = String::from("client_id=");
        let mut val = self.config.get("access").unwrap().to_string();

        // need to remove the quotes from the json - probably the easiest way to do this.
        val.remove(0);
        val.pop();

        return key + &*val
    }

    #[tokio::main]
    pub async fn save_images(self)->Result<(),reqwest::Error>{


        /*

        let res = reqwest::get(url.unwrap().to_string()).await?;
        let image_bytes = res.bytes().await?;

        let mut img = image::load_from_memory(&image_bytes).unwrap();

        let width = img.width();
        let height = img.height();

        let rgb = img.to_rgba8();

        rgb.save(working_dir.to_str().unwrap());
         */

        Ok(())
    }

    /// pull photos from editorial feed.
    #[tokio::main]
    pub async fn pull_editorial(self)->Result<i32,reqwest::Error>{

        // we want to order photos by popularity so prepare string
        let order_by = String::from("popular");

        // get 20 items per page
        let per_page = String::from("20");

        // get the client id we're using to make this request.
        let client_id = self.get_client_id();

        // construct the final request url
        let mut fin = self.base_url + "/photos?" +  &*client_id + "&per_page=" + &*per_page + "&order_by=" + &*order_by;

        // make the request
        let res = reqwest::get(fin).await?;
        let body = res.text().await?;

        // convert body to Json value.
        let json:serde_json::Value =  serde_json::from_str(&body[..]).expect("JSON not well formatted");

        // loop through json to download photos
        if json.is_array() {

            let arr = json.as_array().unwrap();

            for i in 0..arr.len(){

                let mut working_dir = std::env::current_dir().unwrap();
                working_dir.push("images");
                working_dir.push(format!("image{}.jpg",i));

                println!("Will attempt to save image in {}",working_dir.display());

                // get the url to the raw image, we'll resize it later.
                let url = arr[i].get("urls").unwrap().get("raw");

                let mut url_string = url.unwrap().to_string();
                url_string.remove(0);
                url_string.pop();

                println!("image url is {} \n",url_string);

                let res = reqwest::get(url_string).await?;
                let image_bytes = res.bytes().await?;

                let mut img = image::load_from_memory(&image_bytes).unwrap();

                let width = img.width();
                let height = img.height();

                image::save_buffer(&Path::new(working_dir.to_str().unwrap()),img.as_bytes(),width,height,image::ColorType::Rgb8);
            }
        }

        Ok(3)
    }
}