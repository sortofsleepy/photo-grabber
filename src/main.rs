use std::fs::File;
use serde_json::{from_str};
use std::io::Read;
use crate::unsplash::Unsplash;

mod unsplash;

/// loads a json configuration file.
/// Pass a path to the file. Note that path should not start with any kind of slash.
fn load_configuration(path:&str)->serde_json::Value{

    // get working directory
    let mut working_dir = std::env::current_dir().unwrap();

    // important! - ensure path does not have slashes so things get converted to proper OS path format, otherwise
    // path will turn current working directory into root directory
    working_dir.push(path);

    // build file object and open
    let mut file = File::open(working_dir.to_str().unwrap()).expect("File not found");

    // read file to string
    let mut config_string = String::new();
    file.read_to_string(&mut config_string);

    // convert string to str slice
    config_string.to_owned();
    let config_slice = &config_string[..];

    //  return Json value
    serde_json::from_str(config_slice).expect("JSON not well formatted")
}




fn main(){
    let config = load_configuration("config.json");

    // load unsplash api
    let mut us = Unsplash::new(config);


    us.pull_editorial();

}
